var express = require('express');
const morgan = require('morgan');
const path = require('path');
// app is entire express framework
var port = 9090;
require('./db');
// 
var app = express();
app.set('port', 8000);


// socket stuff////
const socket = require('socket.io');
const io = socket(app.listen(8001));
const users = [];
io.on('connection', function (client) {
    console.log('client connected to socket server');
    var id = client.id;
    client.on('new-user', function (data) {
        client.emit('hello', 'hello from node');
        users.push({
            name: data,
            id: id
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    });
    client.on('new-msg', function (data) {
        client.emit('reply-msg', data) // emit is for only client from where event is triggered
        client.broadcast.to(data.receiverId).emit('reply-msg-user', data);// brodcast .emit is for all other client expect requester
    });

    client.on('is-typing', function (data) {
        client.broadcast.to(data.receiverId).emit('typing', data);
    })
    client.on('is-typing-stop', function (data) {
        client.broadcast.to(data.receiverId).emit('typing-stop');
    })

    client.on('disconnect', function () {
        users.forEach(function (user, i) {
            if (user.id === id) {
                users.splice(i, 1);
            }
        });
        client.broadcast.emit('users', users);
    })
});
// socket stuff////
// template engine setup
const pug = require('pug');
app.set('view engine', pug);
app.set('views', path.join(__dirname, 'templates'));


// load routing files
const apiRoute = require('./controllers/api.route');
// load thirid party middleware
app.use(morgan('dev'));

// inbuilt middleware
// app.use(express.static('files')); // within express app locally serve
app.use('/file', express.static(path.join(__dirname, 'uploads'))); // for client
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());
// routing configuration
// routing level middleware 
// if express ko router as an middleware use vako cha vane tyo routing level middleware
app.use('/api', apiRoute);

app.use(function (req, res, next) {
    // 404 error handler
    console.log('i am 404 error handler middleware');
    next({
        msg: 'Not Found',
        status: 404
    });
});

// this is error handling middleware
// this middleware will not be present in http request response cycle
// this middleware must be called to execute
// calling next with argument will execute error handling middleware
// argument passed in next is value received in 1st place holder(err)
app.use(function (err, req, res, next) {
    console.log(' i am error handling  middleware', err);
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    });
});

app.listen(app.get('port'), function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + app.get('port'));
        console.log('press CTRL + C to exit')
    }
});



// middleware
// middleware is a function which has access to http request object, http response object and next middleware refrence

// syntax
// function(req,res,next){

// }
// config block
// express (app) .use()
// app.use(function(req,res,next){

// });
// the order of middleware matters
// types of middleware
// 1. application level middleware
// 2. thirdparty middleware
// 3. inbuilt middleware
// 4. error handling middleware
// 5. routing level middleware

