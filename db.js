const mongoose = require('mongoose');
let dbURL;
if (process.env.REMOTE) {
    dbURL = 'mongodb://group16:group16@ds044979.mlab.com:44979/group16db'
} else {
    dbURL = 'mongodb://localhost:27017/group16db';
}
console.log('db url >>', dbURL);
mongoose.connect(dbURL, { useNewUrlParser: true }, function (err, done) {
    if (err) {
        console.log('db connection failed');
    }
    else {
        console.log('db connection success');
    }
});