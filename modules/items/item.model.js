const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: Schema.Types.ObjectID,
        ref: 'user'
    },
    message: String,
    point: Number,
    approvalStatus: Boolean
}, {
        timestamps: true
    });

const ItemSchema = new Schema({
    name: {
        type: String,
        lowercase: true
    },
    description: String,
    tags: [String],
    category: {
        type: String,
        required: true
    },
    price: {
        type: Number
    },
    brand: {
        type: String
    },
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'value', 'quantity']
        },
        discountUnit: String
    },
    model: String,
    size: String,
    quantity: Number,
    image: String,
    orgin: String,
    user: {
        type: Schema.Types.ObjectID,
        ref: 'user'
    },
    addedBy: String,
    reviews: [ReviewSchema]
}, {
        timestamps: true
    });

module.exports = mongoose.model('item', ItemSchema);