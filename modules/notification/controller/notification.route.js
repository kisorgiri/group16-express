const express = require('express');
const router = express.Router();
// const cors = require('cors');

// router.use(cors());
const notificationController = require('../../notification/controller/notification.controller');
const authentication = require('./../../../middlewares/authenticate');
// router.get('/',)
router.put('/:id', authentication, notificationController.updateNotification);
router.post('/search', authentication, notificationController.searchNotificaiton);
router.get('/allSeen', authentication, notificationController.markAllRead);
router.get('/paginate', authentication, notificationController.getNotification);

module.exports = router;