const passwordHash = require('password-hash');

module.exports = function (obj1, obj2) {
    if (obj2.firstName)
        obj1.name = obj2.firstName + ' ' + obj2.lastName;
    if (obj2.username)
        obj1.username = obj2.username;
    if (obj2.password)
        obj1.password = passwordHash.generate(obj2.password);
    if (obj2.email)
        obj1.email = obj2.email;
    if (obj2.address)
        obj1.address = obj2.address;
    if (obj2.phoneNumber)
        obj1.phoneNumber = obj2.phoneNumber;
    if (obj2.date_of_birth)
        obj1.dob = new Date(obj2.date_of_birth);
    if (obj2.role)
        obj1.role = obj2.role;
    return obj1;
}