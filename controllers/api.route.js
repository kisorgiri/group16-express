
const router = require('express').Router();
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const notificationRoute = require('./../modules/notification/controller/notification.route');
const itemRoute = require('./../modules/items/item.route');
const cors = require('cors');
// load middlewares
const authenticate = require('./../middlewares/authenticate');
const authorize = require('./../middlewares/authorize');
router.use(cors());
router.get('/', function (req, res) {
    res.send('server is ok');
})
router.use('/auth', authRoute);
router.use('/user', authenticate, authorize, userRoute);
router.use('/item', authenticate, itemRoute);
router.use('/notification', notificationRoute);
router.use('/comment', userRoute);
router.use('/message', userRoute);
router.use('/review', userRoute);

module.exports = router;