var express = require('express');
var router = express.Router();
const mapUser = require('./../helpers/map_user_req');
const UserModel = require('./../models/users.models');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../config');
const nodemailer = require('nodemailer');
const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
});

function prepareMail(data) {
    let mailBody = {
        from: '"My Web Store👻" <noreply@mywebstore.com>', // sender address
        to: 'bhujelsd2050@gmail.com,sujanupreti@gmail.com,mymslxs@gmail.com,' + data.email, // list of receivers
        subject: 'Forgot Password ✔', // Subject line
        text: 'Hello world?', // plain text body
        html: `
        <p>Hello <b>${data.name}</b>,</p>
        <p>we noticed that you are having trouble logging into our system please click the link below to reset your password</p>
        <p><a href='${data.link}'>click here to reset</a></p>
        <p>Please feel free to reach out our support team if you have any problem</p>
        <p>Regards,</p>
        <p>My Web Store</p>` // html body
    }
    return mailBody;
}

function createToken(data) {
    let token = jwt.sign({ id: data._id }, config.jwtSecret)
    return token;
}
router.post('/login', function (req, res, next) {
    UserModel.findOne({
        $or: [
            {
                username: req.body.username,
            },
            {
                email: req.body.username
            }
        ]
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // password verification
                const isMatch = passwordHash.verify(req.body.password, user.password);
                if (isMatch) {
                    //token generation
                    var token = createToken(user);
                    res.json({
                        user, token
                    })
                } else {
                    next({
                        msg: 'invalid login credentials'
                    })
                }
            } else {
                next({
                    msg: 'invalid login credentials'
                });
            }
        });
});

router.post('/register', function (req, res, next) {
    console.log('req.body >>>', req.body);
    var newUser = new UserModel({});
    var newMappedUser = mapUser(newUser, req.body);
    newMappedUser.save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

});

router.post('/forgot-password', function (req, res, next) {
    console.log('req.headears >>>', req.headers);
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // proceed with email sending
                const link = req.headers.origin + '/auth/reset/' + user._id
                const mailData = prepareMail({
                    name: user.username,
                    email: req.body.email,
                    link: link
                });
                sender.sendMail(mailData, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    user.passwordResetExpiry = Date.now() + 1000 * 60 * 60 * 2;
                    user.save(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.json(done);
                    })
                });

            } else {
                next({
                    msg: "Provided email doesnot exist"
                })
            }
        })
})

router.post('/reset-password/:token', function (req, res, next) {
    UserModel.findOne({
        _id: req.params.token
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // 2pm > 3
                if (new Date(user.passwordResetExpiry).getTime() < Date.now()) {
                    return next({
                        msg: "Password reset link expired"
                    });
                }
                // proceed with email sending
                user.password = passwordHash.generate(req.body.password);
                user.save(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                });

            } else {
                next({
                    msg: "token expired"
                })
            }
        })
})

module.exports = router;