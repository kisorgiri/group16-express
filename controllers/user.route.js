const router = require('express').Router();
const UserModel = require('./../models/users.models');
const mapUser = require('./../helpers/map_user_req');
module.exports = function (arg1, arg2) {

    router.route('/')
        .get(function (req, res, next) {
            UserModel
                .find({}, {
                    password: 0
                })
                .sort({ _id: -1 })
                // .limit(2)
                // .skip(1)
                .exec(function (err, users) {
                    if (err) {
                        return next(err);
                    }
                    res.json(users);
                });
        })
        .post(function (req, res, next) {

        })
        .put(function (req, res, next) {

        })
        .delete(function (req, res, next) {

        });
    router.route('/profile')
        .get(function (req, res, next) {
            res.end('from user profile router')
        })
        .post(function (req, res, next) {
            console.log('at user profile posst page');
            res.send('dashboard should be here');
        })
        .put(function (req, res, next) {

        })
        .delete(function (req, res, next) {

        });
    router.route('/change-password')
        .get(function (req, res, next) {

        })
        .post(function (req, res, next) {

        })
        .put(function (req, res, next) {

        })
        .delete(function (req, res, next) {

        });

    router.route('/:id')
        .get(function (req, res, next) {
            UserModel.findOne({ _id: req.params.id })
                .then(function (data) {
                    res.json(data);
                })
                .catch(function (err) {
                    next(err);
                });
            // UserModel.findById(req.params.id, function (err, user) {
            //     if (err) {
            //         return next(err);
            //     }
            //     res.json(user);
            // })

        })
        .put(function (req, res, next) {
            var id = req.params.id;
            UserModel.findById(id, function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    user.name = req.body.name;
                    var updatedMapUser = mapUser(user, req.body);
                    updatedMapUser.save(function (err, updated) {
                        if (err) {
                            return next(err);
                        }
                        res.json(updated);
                    })

                }
                else {
                    next({
                        msg: 'User not found'
                    })
                }
            })
        })
        .delete(function (req, res, next) {
            UserModel
                .findById(req.params.id)
                .exec(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        user.remove(function (err, removed) {
                            if (err) {
                                return next(err);
                            }
                            res.json(removed);
                        })
                    }
                    else {
                        next({
                            msg: 'User not found',
                            status: 404
                        })
                    }
                })
        });

    return router;
}

