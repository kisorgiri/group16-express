
// to connect with mongo server
// mongo
// server default port is 27017

// mongo
//  > input interface mongo shell command are executed
// shell commands
// show dbs // list all the datbase of system

// use <db_name> // if (db_name exist){select existing db} else { create new db and select it}

// db // (selected db  check)

// show collections // list all the collection of selected db

// db.<col_name>.insert({valid json object}) // insert document  (C) insertMany for arrays
// db.<col_name>.find({query_builder}) // fetch documents  (R)
// db.<col_name>.update({query_builder},{$set:{updated Obj}},{options eg multi upsert});
// eg 
// db.users.update({role:1},{$set:{position:'admin'}},{multi:true}); (U)
 
// db.users.remove({condition applied to remove documents});

// drop collection
// db.<col_name>.drop(); // it will drop given collection

// drop database
// db.dropDatabase();

// benefits of using ODM (object document modeling
// database is not programming language specific
// ODM/ORM is always programming language specific
// mongoose is mongod db ODM for nodejs
// 1 schema based solution 
// 2 data types declaration
// 3 validation (indexing) unique, required
// 4 ODM methods are very useful
// 5 middleware 

//####################### MONGODB BACK UP AND RESTORE ###########/
// two way of backup and restore
// bson format (binary json)
// human redable format (CSV, JSON);

// 1 bson struture
// back up command
// mongodump // this command will create a default dump with all the database of system
// mongodump --db <db_name> it will dump the selected database in dump folder
// mongodump -d <db_name> it will dump the selected database in dump folder
// mongodump --db <db_name> --out <path_to_output_directory>
// mongodump -d <db_name> -o <path_to_output_directory>
//####################### MONGODB BACK UP AND RESTORE ###########/

// restore command
// mongorestore
// mongorestore // it will restore all the database from dump folder
// mongorestore will search for default dump folder
// mongorestore path_to_db_back
// mongorestore --drop // drop duplicate documents


// 2 json and csv
// 1 json
// backup command
// mongoexport
// mongoexport --db <db_name> --collection <col_name> --out <path_to_destination_folder_with.json_extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_destination_folder_with.json_extension>

// restore command
// mongoimport
// mongoimport --db<db_name> --collection<col_name> path_to_json_file
// mongoimport -d<db_name> -c<col_name> path_to_json_file

// mongodump and mongorestore
// mongoexport and mongoimport 
// these command always came in pair

// csv
// backup command
// mongoexport --db <db_name> --collection <col_name> --type=csv --fileds 'coma_seperated_value '  --out <path_to_outPut_directory_with.csv_extension>

// restore command
// mongoimport
// mongoimport --db<db_name> --collection <col_name> --type=csv path_to_csv_containg_file  --headerline